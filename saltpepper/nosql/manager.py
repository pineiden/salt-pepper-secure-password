from datadbs.rethinkdb import RethinkDB
import rethinkdb as rdb
from datetime import tzinfo, timedelta, datetime
from networktools.library import my_random_string
import random

import asyncio

min_length = 15
max_length = 60


class RethinkDBPepper:
    def __init__(self,
                  *args,
                  **kwargs):
        # create bridge instace
        #bprint("Station %s and port %s" % (self.stations[ids]['code'], self.bridge))
        #gprint("Bridge: %s" % format(self.bridge))
        #local_port = self.create_bridge(ids)
        #rprint("Local port %s" %local_port)
        print("Init")
        code = kwargs.get('code', 'pepper')
        hostname = kwargs.get('hostname', 'localhost')
        port = kwargs.get('port', 6937)
        database = kwargs.get('database', 'dressing')
        table = kwargs.get('table', 'pepper')
        loop = asyncio.get_event_loop()
        #idd = self.get_id_by_code('DBDATA', code_db)
        # bprint("The db_data's id: %s" % idd)
        self.db_datos = dict(host=hostname,
                        port=port,
                        address=(hostname, port),
                        code=code,
                        dbname=database,
                        io_loop=loop,
                        table=table,
                        env='pepper_%s' %code)
        print("Data")
        print(self.db_datos)
        name='RethinkDB'
        try:
            self.rethinkdb = RethinkDB(**self.db_datos)
            print("Created rethinkdb")
        except Exception as exec:
            print("Error al inicializar conexión %s" %exec)
            raise exec
        # self.common[code] = dict()
        # self.sta_init[ids]=True

    def up_session(self):
        self.rethinkdb = RethinkDB(**self.db_datos)
        print("Session renew")
        print(self.rethinkdb.session)

    def close(self):
        self.rethinkdb.close()

    async def create_db_table(self):
        r = self.rethinkdb
        conn = await r.async_connect()
        dbname = self.db_datos.get('dbname')
        r.set_defaultdb(dbname)
        self.dbs = await r.list_dbs()
        await r.select_db(dbname)
        table_name = self.db_datos.get('table') 
        # rprint("Pre ending instance")
        await r.create_table(table_name, dbname)
        self.indexes_list = await r.get_indexes(table_name)
        self.table_list = await r.list_tables(r.default_db)
        # create index to this pepper table
        index_name = 'salt'
        await r.create_index(table_name, index_name, dbname)
        index_name = 'username'
        await r.create_index(table_name, index_name, dbname)

    async def check_salt(self, salt):
        ## Check if this code is on the table
        table_name = self.db_datos.get('table')
        index_name = 'salt'
        print("Salt")
        print(salt)
        data = await self.rethinkdb.get_data(table_name, index_name, salt)
        print("New data")

        data_list = []
        while (await data.fetch_next()):
            elem =  await data.next()
            data_list.append(elem)        
        print(data_list)
        if data_list:
            return True
        else:
            return False

    async def save_salt(self, data):
        table_name = self.db_datos.get('table') 
        return await self.rethinkdb.save_data(table_name, data)

    async def get_salt(self, username):
        table_name = self.db_datos.get('table')
        index_name = 'username'
        data = await self.rethinkdb.get_data(table_name, index_name, username)
        data_list = []
        while (await data.fetch_next()):
            elem = await data.next()
            if elem:
                data_list.append(elem)        
        print(data_list)
        return data_list

    async def generate_salt(self, username):
        e_user = await self.get_salt(username)
        if not e_user:
            uin = random.randrange(min_length, max_length)
            value = my_random_string(uin)
            result = await self.check_salt(value)
            while result:
                value = my_random_string(uin)
                result = await self.check_salt(value)
            data = {'username': username, 'salt': value}
            result = await self.save_salt(data)
            return data, result
        else:
            return e_user[0], None


    async def update_salt(self, username):
        table_name = self.db_datos.get('table')
        await self.delete_salt(username)
        data, result = await self.generate_salt(username)
        return data, result


    async def delete_salt(self, username):
        table_name = self.db_datos.get('table')
        await self.rethinkdb.delete_data(table_name, {'username':username})


PepperManager = RethinkDBPepper
