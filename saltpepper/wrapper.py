from .sql.manager import SessionPepper
from .nosql.manager import RethinkDBPepper
import asyncio

class PepperWrapper:
    engines = {
        'sql': SessionPepper,
        'nosql': RethinkDBPepper
    }

    def __init__(self, engine_name, **kwargs):
        self.engine = self.engines.get(engine_name)(**kwargs)
        self.kind = 'asyncio' if engine_name=='nosql' else 'normal'
        self.is_async = False
        if self.kind == 'asyncio':
            self.loop = asyncio.get_event_loop()
            self.is_async = True

    def get_engine(self):
        return self.engine

    def get_kind(self):
        return self.kind

    def is_async(self):
        return self.is_async

    # generate salt and save:

    def gen_salt(self, username):
        if self.is_async:
            return self.loop.run_until_complete(self.engine.generate_salt(username))
        else:
            return self.engine.pepper(user=username)
 
    # update salt

    def up_salt(self, username, new_data):
        if self.is_async:
            return self.loop.run_until_complete(self.engine.update_salt(username))
        else:
            id_user = self.engine.get_pepper_id(username)
            user = self.engine.get_pepper_by_id(id_user)
            return self.engine.update_pepper(user, new_data)

    def del_salt(self, username):
        if self.is_async:
            return self.loop.run_until_complete(self.engine.delete_salt(username))
        else:
            id_user = self.engine.get_pepper_id(username)
            user = self.engine.get_pepper_by_id(id_user)
            return self.engine.delete_pepper(user)

    def get_salt(self, username):
        if self.is_async:
            return self.loop.run_until_complete(self.engine.get_salt(username))
        else:
            id_user = self.engine.get_pepper_id(username)
            user = self.engine.get_pepper_by_id(id_user)
            return user
