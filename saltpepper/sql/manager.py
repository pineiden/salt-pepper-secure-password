from sqlalchemy import update
from sqlalchemy import Table, MetaData
from sqlalchemy.sql import text
from sqlalchemy.sql import select
from sqlalchemy.sql import table
from sqlalchemy.sql import column
from sqlalchemy import inspect
import datetime

try:
    from models import Pepper
except Exception:
    from .models import Pepper

try:
    from db_session import session, engine, connection
except Exception:
    from .db_session import session, engine, connection


def object_as_dict(obj):
    return {c.key: getattr(obj, c.key)
            for c in inspect(obj).mapper.column_attrs}


class SessionHandle(object):
    """
    A session middleware class to manage the basic elements in the database,
    has generic methods to verify the elements existence, update_ and obtain
    lists from tables
    """

    def __init__(self, *args, **kwargs):
        self.session = session
        self.conn = connection
        self.metadata = MetaData()

    def close(self):
        self.session.close()

    def exists_table(self, table_name, **kwargs):
        """
        Check if table_name exists on schema
        :param table_name: a table_name string
        :return: boolean {True,False}
        """
        this_schema = kwargs.get('schema', 'public')
        return engine.dialect.has_table(engine.connect(), table_name)

    def exists_field(self, table_name, field_name, **kwargs):
        """
        Check if field exist in table
        :param table_name: table name string
        :param field_name: field name string
        :return:  bolean {True, False}
        """
        this_schema = kwargs.get('schema', 'public')
        assert self.exists_table(table_name), 'No existe esta tabla'
        fields = Table(
            table_name,
            self.metadata,
            autoload=True,
            autoload_with=engine,
            schema=this_schema)
        r = [c.name for c in fields.columns]
        try:
            r.remove('id')
        except ValueError:
            pass
        assert field_name in r, 'No existe este campo en tabla'
        return True

    def value_type(self, table_name, field_name, value,**kwargs):
        """
        Check if value is the same value type in field
        :param table_name: table name string
        :param field_name: field name string
        :param value:  some value
        :return: boolean {True, False, None}; None if value type doesn't exist
        on that list, because there are only the most common types.
        """
        this_schema = kwargs.get('schema', 'public')
        # get type value
        assert self.exists_table(table_name), 'No existe esta tabla'
        fields = Table(
            table_name,
            self.metadata,
            autoload=True,
            autoload_with=engine,
            schema=this_schema)
        r = [c.name for c in fields.columns]
        assert self.exists_field(table_name, field_name), \
            'Campo no existe en tabla'
        this_index = r.index(field_name)
        t = [str(c.type) for c in fields.columns]
        this_type = t[this_index]
        b = False
        if this_type == 'INTEGER' or this_type == 'BigInteger':
            assert isinstance(value, int)
            b = True
        elif this_type[0:7] == 'VARCHAR' or this_type == 'TEXT' or this_type == 'STRING':
            assert isinstance(value, str)
            b = True
        elif this_type == 'BOOLEAN':
            assert isinstance(value, bool)
            b = True
        elif this_type == 'DATE':
            assert isinstance(value, datetime.date)
            b = True
        elif this_type == 'DATETIME':
            assert isinstance(value, datetime.datetime)
            b = True
        elif this_type == 'FLOAT' or this_type == 'NUMERIC':
            assert isinstance(value, float)
            b = True
        else:
            b = None

        return b
        # check

        def generic_query(self, st):
            q = text(st)
            u = self.session.execute(q)
            return(u)

    def update_table(self, table_name, instance, field_name, value, **kwargs):
        """
        Change some value in table
        :param table_name: table name class
        :param instance: instance to modify in database
        :param field: field name string
        :param value: value
        :return: void()
        """
        this_schema = kwargs.get('schema', 'public')
        # update to database
        table = Table(
            table_name,
            self.metadata,
            autoload=True,
            autoload_with=engine,
            schema=this_schema
        )
        # print(table+":"+field_name+":"+value)
        up = update(table).where(
            table.c.id == instance.id).values({field_name: value})
        print(up)
        self.session.execute(up)
        self.session.commit()

    # LIST ELEMENTS

    def get_list(self, model):
        """
        Get the complete list of elements in some Model Class (Table in db)

        :param model:Model Class
        :return: a query list
        """
        return self.session.query(model).all()

# Class Alias
SH = SessionHandle

class SessionPepper(SH):
    """
    An specific SessionHandler who extends database model in Collector case,
    has a Station, a Protocol and a FBData tables
    """

    # STATION TABLE

    def pepper(self, **kwargs):
        user = kwargs.get('user')
        instance = None
        if user:
            instance = self.session.query(Pepper).filter_by(user=user).first()
        if instance:
            return instance
        else:
            pepper = Pepper(user=user)
            self.create_pepper(pepper)
            return pepper

    def create_pepper(self, pepper):
        self.session.add(pepper)
        self.session.commit()

    def update_pepper(self, instance, new_dict):
        print("Actualizar pepper %s" % instance.user)
        t_name = 'pepper'
        pepper = object_as_dict(instance)
        db=None
        protocol=None
        new_user = new_dict.get('user', None)
        if_exists_user = self.get_pepper_id(new_dict.get('user'))
        if new_user and not if_exists_user:
            self.update_table(t_name, instance, 'user', new_user)

    def delete_pepper(self, pepper):
        self.session.delete(pepper)
        self.session.flush()

    def get_peppers(self):
        stmt=select([column('id'),
                     column('user'),
                     column('code')]).select_from(table('pepper'))

        peppers=self.get_list(Pepper)
        #for sta in stations:
        new_peppers=peppers
        return new_peppers

    def get_pepper_id(self, user):
        # code is a unique value
        u = self.session.query(Pepper).filter_by(user=user).first()
        if u:
            return u.id
        else:
            return None

    def get_pepper_by_id(self, pk):
        u = self.session.query(Pepper).filter_by(id=pk).first()
        if u:
            return u
        else:
            return None


PepperManager = SessionPepper
