from sqlalchemy import create_engine
from sqlalchemy.schema import CreateSchema

try:
    from .models import Base
except Exception:
    from models import Base

from db_session import engine, session

print("Creating Base Model")
result=Base.metadata.create_all(engine, checkfirst=True)
print("Created Base Model")
print(result)
session.commit()
