from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine
from networktools.environment import get_env_variable


data_db=dict(
	user=get_env_variable("PEPPER_USER"),
	passw=get_env_variable("PEPPER_PASSW"),
	hostname=get_env_variable("PEPPER_HOST"),
	port=get_env_variable("PEPPER_PORT"),
	dbname=get_env_variable("PEPPER_DBNAME"),)


class PepperSession:
    def __init__(self, *args, **kwargs):
        self.db_engine ='postgresql://{user}:{passw}@{hostname}:{port}/{dbname}'.format(**kwargs)
        self.engine = create_engine(self.db_engine)
        self.connection = self.engine.connect()
        self.session = sessionmaker(bind=self.engine)()

    def get_session(self):
        return self.session

print(data_db)

csession = PepperSession(**data_db)
session = csession.get_session()
engine = csession.engine
connection = csession.connection

print(csession, session, engine, connection)
