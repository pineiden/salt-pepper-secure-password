from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import (Column, Integer, Text, String) 
import json 


from networktools.library import my_random_string
from .db_session import session
import random

min_len = 15
max_len = 200

def get_code(context):
    print("Context")
    print(context)
    uin = random.randrange(min_len, max_len)
    # option: session.query(model).all()
    stmt_check ='select to_regclass(\'public.pepper\')'
    set_tables = [r for r, in session.execute(stmt_check)]
    if set_tables[0]:
        stmt =  "select EXISTS (select code from public. pepper)"
        # try: zip(*result)[0]
        result = random.shuffle([r for r, in session.execute(stmt)])
        print("Result...")
        print(result)
        value = my_random_string(uin)
        if isinstance(result, list):
            while value in result:
                value = my_random_string(uin)
        return value

Base = declarative_base()

class Pepper(Base):
    """
    Define salt hash to add to passwords
    """

    __tablename__ = 'pepper'
    __table_args_ = {'schema': 'public'}

    id = Column(Integer, primary_key=True, autoincrement=True)
    user = Column(String(20), unique=True)
    code = Column(String(max_len), default=get_code, onupdate=get_code)

    def __str__(self):
        return str(self.id) + " :" + str(self.user) + " :" + str(self.code)

    def __repr__(self):
        return "<%s:%s:%s>"%(self.id, self.user, self.code)
