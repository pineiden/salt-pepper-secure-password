# Crear database
create database pimienta;
# Crear rol o usuario
create role sal;
# Asignar password a usuario
alter role sal with password 'aliño';
# Asignar usuario a base de datos con ciertos privilegios
grant all privileges on database pimienta to sal;
# Habilitar que usuario pueda hacer login
alter role sal with login;
# Conectar a la nueva base de datos
\connect pimienta;
# Habilitar permisos a usuarios sobre tablas
CREATE SCHEMA  IF NOT EXISTS pepper AUTHORIZATION sal;
GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA pepper TO sal;
