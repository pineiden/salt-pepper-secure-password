from sqlalchemy import Text, TypeDecorator
from networktools.library import my_random_string
from db_session import session
import random

min_length = 15
max_length = 60

class PepperField(TypeDecorator):
    impl = Text

    def __init__(self, max_length=max_length, **keweargs):
        self.min_len = min_length
        self.max_len = max_length
        self.code = self.get_code(max_length)
        super(PepperField, self).__init__(**keweargs)

    def get_code(self, uin):
        uin = random.randrange(self.min_len, self.max_len)
        # option: session.query(model).all()
        stmt_check ='select to_regclass(\'pepper.pepper\')'
        set_tables = [r for r, in session.execute(stmt_check)]
        if set_tables[0]:
            stmt =  "select EXISTS (select code from pepper.pepper)"
            # try: zip(*result)[0]
            result = random.shuffle([r for r, in session.execute(stmt)])
            value = my_random_string(uin)
            while value in result:
                value = my_random_string(uin)
            return value
        return None
		
    def process_bind_param(self, value, dialect):
        """Ensure the value is a PepperField and return the code"""
        return self._convert(value).code

    def process_result_value(self, value, dialect):
        """Convert the hash to a PepperField, if it's non-NULL."""
        return self._convert(value).code

    def _convert(self, value):
        """Returns a PepperField from the given string.
        """
        if isinstance(value, int):
            return PepperField(max_length=value)
        elif not isinstance(value, int):
            return PepperField()

