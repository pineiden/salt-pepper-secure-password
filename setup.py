from setuptools import setup

file_req = './requeriments.txt'


setup(name='saltpepper',
      version='0.1',
      description='A module that allow the creation the salt code for secure passwords',
      url='https://gitlab.com/pineiden/salt-pepper-secure-password',
      author='David Pineda Osorio',
      author_email='dpineda@uchile.cl',
      license='MIT',
      packages=[],
      zip_safe=False)
