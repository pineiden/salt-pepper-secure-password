from saltpepper.wrapper import PepperWrapper
from networktools.colorprint import gprint, bprint, rprint


users = ['guillermina', 'pineiden', 'huatepec']

rprint("="*30)
bprint("Test SQL mode")
rprint("="*30)

engine = 'sql'
pepper = PepperWrapper(engine)


list_users = [pepper.gen_salt(user) for user in users]

[bprint(data) for data in list_users]


rprint("="*30)
bprint("Test SQL update data")
rprint("="*30)

username = 'guillermina'
new_data = {'user': 'guillerma'}


updated_user = pepper.up_salt(username, new_data)

position=users.index(username)
users[position] = 'guillermina'

bprint("Updated user")
gprint(updated_user)


rprint("="*30)
bprint("Test SQL obtener data")
rprint("="*30)


data = pepper.get_salt('pineiden')

bprint("Info user pineiden")
gprint(data)



rprint("="*30)
bprint("Test SQL delete data")
rprint("="*30)

data = pepper.get_salt('huatepec')

bprint("Info deleted user huatepect")
gprint(data)

new_all_list = [pepper.get_salt(user) for user in users]

bprint("Info deleted user all users")
[gprint(data) for data in new_all_list]
