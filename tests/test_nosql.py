from saltpepper.nosql.manager import RethinkDBPepper
from networktools.colorprint import gprint, bprint, rprint
import asyncio

users = ['pineiden', 'margarita', 'pachodeltambo']

manager_pepper = RethinkDBPepper()

print(manager_pepper)
print(manager_pepper.db_datos)
# Create the salt for every user


async def run_manager(manager):
    await manager.create_db_table()
    rprint("="*30)
    bprint("Creating salt for user")
    rprint("="*30)
    list_data = []
    for user in users:
        data = await manager.generate_salt(user)
        list_data.append(data)
    [bprint(data) for data in list_data]
    await asyncio.sleep(5)
    rprint("="*30)
    bprint("Updating salt on users")
    rprint("="*30)
    up_salt = lambda user: manager.update_salt(user)
    new_list_data = []
    for user in users:
        data = await manager.update_salt(user)
        new_list_data.append(data)
    [bprint(data) for data in new_list_data]
    await asyncio.sleep(5)
    rprint("="*30)
    bprint("Delete user pineiden")
    rprint("="*30)
    await manager.delete_salt('pineiden')
    rprint("="*30)
    bprint("List all")
    rprint("="*30)
    last_list_data = []
    for user in users:
        data = await manager.get_salt(user)
        if data:
            last_list_data.append(data[0])
    [bprint(data) for data in last_list_data]


task = run_manager(manager_pepper)
loop = asyncio.get_event_loop()
loop.run_until_complete(task)
loop.close()
